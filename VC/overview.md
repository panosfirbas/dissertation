





# Introduction! (Related Work, Theoretical framework , Literature review)
## Exploring Cis-Regulation with modern techniques
### What genes are active
    Genome assembly
    RNAseq
    CAGEseq
### What regions are CREs?
    CHIP histones
    DNAse, ATACseq
### What TF binds where?
    CHIP-TF
    PWMs
    Footprints
### Which CREs does a gene interact with, and vice-versa
    TADs, TAD borders
    4C, HI-Chip, etc
### Other genomic features:
    Nucleosome positioning
    ++
## The evolution of genomic landscapes of vertebrates (Organisms)
### Zebrafish
    As a model organism
    Relevance in researching: Development, Evolution, Disease, Cis-Reg, Trans-Reg
### Amphioxus
    As a model organism
    Relevance in researching: Development, Evolution, Disease, Cis-Reg, Trans-Reg
    #### (Cephalo)chordates
### Other Organisms
    Medaka
    Mouse
### WGD
    Whole Genome Duplications and their evolutionary relevance
### Evolut. questions
    Specialization
    Subfunctionalization
    That model thing the lady asked @ Carmona
## Gathering Data (Characterizing the genomes)
## (Results from lab, Results of BioInfo, some simple (public) data gathering)
    # Collect data in zebrafish and amphioxus (medaka, mouse, other? per case?)
        Give super brief overview for each:
            e.g. "We found 100000 peaks"
        The idea is to end up in each section with a feeling of "okay we have this data for this organism in that stage".
        # Genomes
        # RNA
        # Histone-CHIPs
        # ATAC
        # TF-Chips
        # PWMs
        # Footprints
        # C-techniques
        
# Explore the Data, Gaining insights, Evo Questions
    # Explore data in zebrafish and amphioxus
        We can now use the data to say more, for example
        # Genomes
            Sizes of genomes
            Fragmentation
            Inter-TSS distances
            Compare the species
            Gene Families
        # RNA
            How many active genes
            WGCNA ?
            MFUZZ ?
            Compare the species
                Heatmaps
        # Histone-CHIPs
            X per cent of the genome is covered, X-overlap with Y,Z
            Compare the species
        # ATAC
            Size distributions
            Downsampling effects
            Such and such peaks
                per intergenic,
                per Gene Family
                per GREAT
                    Daltons etc
                per promoter?
                per high CHIP region?
            ++
            Compare the species
        # TF-Chips
            ??
            Compare the species
        # PWMs
            Simons Clusters set
            Homer set
            Mapping
            Overlaps with other signals?
            Compare the species
        # Footprints
            ?
            Compare the species
        # C-techniques
            ?
            Compare the species

# Conclusion, Discussion, Evaluation
    # I guess I'll quit here
# Summary, Further Work, Reflection
    # Kill me now
# Methods, Implementation
    #(How we did what we did, the easiest part of the thing)



