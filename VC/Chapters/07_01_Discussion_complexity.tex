\label{chapter:complexity}
We might have assumed, at some point in history, that humans have the biggest and most
complex genome in life. We are after all the center of our own universe, the chosen
children, our genome would of course be special.

Unfortunately for our pride, it turns out that even the humble onion has a genome many
times bigger than ours. It is to be concluded then that organism complexity is not
directly reflected on genome size. 

As far as our model organisms are concerned, amphioxus has a smaller genome than the
vertebrates (Chapter \ref{chapter:thegenomes}). The additional genomic space in
vertebrates is not, of course found isolated in some corner of the genome, but rather
is
distributed between the genes. This is evident by the fact that vertebrate genes have
larger genomic spaces around them and their putative CREs are found in greater distances
from any TSS (Chapters \ref{chapter:theintergenicregions} and 
\ref{chapter:thegreatregions}). In the microcosm of our work, an increase in genome
size comes with an increase in complexity, although no meaningful conclusions could be
drawn from such a small scope.

But what is animal complexity? 

Complex is defined as something composed of two or more parts, the more parts the higher
the complexity. In terms of animals then, complexity could be measured in terms of
distinct tissues or organs or cell lines. Vertebrates have evolved plenty of new elaborations on the chordate body plan, and all the
necessary new tissues, cell lines and developmental programs that are needed.
A head with predatory jaws, complex sensory organs and complex brain, a skeleton are all
additions to the ancestral body plan.They are formed during development when the organism consists of a relatively small
number of structures, which interact to give rise to more structures, which in turn give
rise to more, in a widening cascade (read about developmental-depth in \cite{mcshea98}). 
Our understanding is that novelties in gene regulation can lead to novel cell states, or cell
lines. These new cell lines interact with the existing developmental context to produce
new structures which lead to morphological novelties.

New cell populations like the ectodermal placodes or the neural crest have been implicated
in the evolution of tissue innovations in vertebrates \cite{Arendt2008}.
Additionally the neural crest was linked to the recruitment of ancestral regulatory
genes, an observation that connects regulatory diversification to cell type
diversification to tissue diversification.


% Emerging predatory jaws and elaborated sensory organs, together with the acquisition of both an endoskeleton and an increasingly complex brain, may have permitted the transition from a filter- feeding to an active predation lifestyle

% There is,nevertheless, a number of interesting observations to be made on the influence of
% genome size on organisms. Genome size, if not causally connected, is correlated with
% changes in cellular parameters such as cell size and cell proliferation\cite{gsize0}. 
% This puts it under evolutionary selection since perturbations of such cell mechanics
% during development can lead to significant differences in animal form.

% Genome size is affected by a variety of factors such as WGD or polyploidy, and one of
% the most important drivers of genome expansion appears to be the proliferation of
% transposable elements, especially so in ray-finned fishes\cite{gsize1}.

% Amphioxus does carry a measurable amount of transposable element proliferation in its
% genome, a bit less than zebrafish and mouse where almost half of the genome is covered by
% TEs but more than medaka where TEs are almost non-existant (Chapter 
% \ref{chapter:thegenomes}).

% Teleosts have relatively small genomes\cite{gsize1}. Our two fish models,despite the
% third WGD event in their evolutionary history have a smaller genome size than mouse.
% Amphioxus doesn't have any duplications and although we wouldn't necessarily expect it to
% be the case, has a significantly smaller genome as well (Chapter \ref{chapter:thegenomes}).
% Such a structure, or tissue, is defined by its
% proteomic state, which is define by its transcriptomic state; which genes are expressed
% and how much. This in turn is controlled by the gene regulatory networks (GRNs) that are
% active in the cells.



\subsection{Complexity and WGD}

The regulatory landscape in an organism that has just underwent a WGD event brims with
evolutionary potential. Many gene copies are now redundant and can be lost, but in the
process of being lost, a gene might change enough to be used in a different function,
therefore regaining its evolutionary purpose. Genes that are kept in duplicate can
themselves evolve and differentiate, presumably at different rates.

% It is also interesting to consider what
% happens to the CREs that are themselves copied. If

We have noticed that different categories of genes are retained at different rates.
The general understanding is that genes that are involved in functional modules such as
metabolic pathways, metabolic cascades, regulatory networks, etc. tend to be
preferentially retained. It is suggested that a non stoichiometric production of the
components of these modules will quickly lead to problems for the organism.

Therefore, after a duplication sister genes will be under pressure to maintain a constant
level of transcription, to satisfy the needs of the gene module in which they are
involved\cite{lynch2015}. TFs are among the categories of genes that are known to be
preferentially retained, as
they are often parts of gene regulatory networks which are such functional modules. 
Moreover many TFs are pleiotropic, being involved in multiple regulatory networks,
increasing the importance of conserving their transcription levels.

% functional modules (GFMs). These modules, Freeling and Thomas \cite{freeling06} feel 
% "would be a natural unit to be recruited or co-opted to a new developmental boundary
% during positive selection for morphological adaptations".
% This preferential retention can be seen as an evolutionary drive since itself is
% inevitable and it provides the organism with a big set of genes with a strong
% connection to animal form and that can now be diversified. Like this, WGDs push the limits
% of complexity that an organism can achieve upwards\cite{Semon2007}.
% The preferential retention is arguably because of the need of genes that are involved in
% stoichiometric reactions to be transcribed in proper ratios to each other and the loss of
% one of the copies would significantly imbalance the system.

Let us consider how a WGD effects a TF and a membrane protein as an counter-example.
A TF’s “concentration” on the genome will remain the same after a duplication. In other
words, there is now double the amount of binding sites where the TF is expected and double
the amount of TF protein. The membrane protein’s concentration on the other hand doubles since we
have double the amount of this protein for the more-or-less same area of membrane. The expected
pressure then would be for the TF system to remain as it is, maintaining the TF copies,
and for the additional membrane protein copy to be lost, so that things return to status
quo.

\begin{figure}[!h] % [!h] if you want the figure put HERE, otherwise automagic centering
\includegraphics[width=1\textwidth]{../Figures/Inkscape/TDHKhypothesis2}  
\decoRule 
\caption[WGD effects on different genes]{ The evolutionary pressure on genes copies after
a WGD effect might depend on gene function. After a WGD, if the copy of a TF gene is lost,
the dosage of the TF protein in the system will be halved since the binding sites where
the TF is expected have doubled while the amount of the TF protein won't. This has a
multi-pronged effect on cis-regulation since multiple target genes will be affected.
Consequently there is strong pressure for the TF to be kept in duplicate.     
In contrast, other genes like a cell membrane protein, will be pressed to lose the
redundant copy since that will return the protein to its expected levels.}
\end{figure}

This implicates the second actor of transcription regulation; cis-regulatory elements.
If the transcription of a CRE's target is under pressure to be kept stable, the pressure
would extend to the conservation of the CRE itself. Are CREs preferentially retained? The evident high-turnover of CREs, even for
relatively close species points to the contrary, but our inability to match CREs across
species doesn't mean that the ancestral element wasn't retained after the WGDs. Besides,
overprinting of existing CREs is a rampant phenomenon \cite{Maeso2016a}.

We showed that vertebrates have more putative regulatory elements in total and  per gene 
(chapter\ref{chapter:morepeaks}) and those are further away from the gene's transcription start
sites (chapter \ref{chater:cretssdistances}). A larger number of regulatory elements is to be expected in the larger genomic landscapes
of vertebrates, but we demonstrated that even if we group genes in buckets of
similarly sized landscapes, zebrafish genes contain more CREs in their landscapes than
amphioxus genes (chapter \ref{chapter:morepeaks}), indicating that
cis regulatory complexity increase in vertebrates can be observed both in terms of number
of elements per gene, but also in terms of density of elements in similarly sized regions.

Crucially to our previous arguments, 'trans-dev' labeled genes (TD), genes that are
implicated in transcription and development (many of them TFs), tend to have more CREs in their
landscapes that housekeeping genes. Similarly so for genes that are retained in multiple
copies (many of them TD) versus genes that are kept in a single copy. This fits with the
idea that CREs of
genes that are preferentially retained in several copies would themselves be preferentially
retained. Alternatively, maybe CRE-rich genes are like that because for some reason they
tend to create new CREs more often. Interestingly, the homologous genes in amphioxus, both
of the TD category and generally of genes that are kept in multiple copies, are also
richer in CREs than other amphioxus genes, again fitting with the model of preferential
retention and diversification for CREs, specially so for the CREs of TD genes.

This preferential retention of developmentally crucial elements, both genes and CREs,
pushes the ceiling of complexity for organisms. Before the WGD, at the node of each
regulatory network resided the single copy of a gene. This single copy needs to be
transcribed at certain levels and has some evolutionary 'wiggle-room' but there is no
safety net. After a WGD, two copies will preferentially be kept, alongside their
cis-landscapes and they can now both evolve while relying on their copy to buffer any
differences from the ancestral expression pattern.

In the following chapter we discuss the various patterns in which this diversification of
gene copies can happen.




% Preferentially retention and overprinting is a fitting scenario to explain the increase of
% cis regulatory elements in vertebrates. Genes that are marked as Transcriptionally or
% Developmentally important (TD), including many TFs, are richer both in cis regulatory
% elements than non-TD genes, in both amphioxus and zebrafish. Maybe both organisms started
% accumulating CREs after the split from the LCA and the slow evolving amphioxus accumulated
% less than vertebrates. Transposable element proliferation could have helped to that end,
% but de-novo creation and tandem duplications of CREs seem to be rare\cite{Maeso2016a}.
