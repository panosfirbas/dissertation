% Conservation of cis regulation %   NACC %   Phylotypic %   modules 
Investigating the evolution of cis-regulatory elements is a challenging task.
Conservation in genetics is usually measured in terms of sequence similarities. While
coding sequences do change over evolutionary time, they remain similar enough that gene
homology families can be
constructed and some genes have been observed to be universal in life.
Here we worked with homologous gene families in vertebrates which gave
us more or less 10-20 thousand genes in each species that we can trace in the tree of
vertebrates.

The same approach on non-coding elements yields some conserved elements, but the numbers
are significantly lower. A few thousand have been identified in vertebrates and separately
in fruitflies \cite{engstrom2007genomic,Bradley2010,He2011,Paris2013,Khoueiry2017} or
nematodes \cite{vavouri2007parallel}, 
but only a few dozen have been shown to be conserved between human and amphioxus.

In human and mouse, orthologous CREs can be detected very well thanks to the large degree
of conservation of sequence and synteny \cite{ Berthelot2018, Cotney2013,
Reilly2015,Villar2015, Stergachis2014, Vierstra2014,Zhou2014}. Which is great news since
despite the high rates of repurposing and turnover\cite{Cotney2013, Reilly2015,
Vierstra2014, Odom2007}, cis-regulation research for therapeutic purposed can be readily
investigated in our most important vertebrate model organism\cite{enhd1,polydactyly}.

Thee-way comparisons between
human, fly and nematodes found little conservation of individual regulatory targets and binding
patterns \cite{Boyle2014} and some conservation of fundamental features of transcription
\cite{Gerstein2014}. Fruitflies and nematodes have been shown to be highly derived
\cite{Hendrich2003,Irimia2012, Simakov2012} so maybe the lack of CRE conservation should not
come as a surprise. 

Again, the turnover rates of CREs are much higher than coding sequences, but they can also
vary between tissues or cell types \cite{Vierstra2014, Nord2013}, developmental stages or
types of CREs \cite{Cotney2013, Reilly2015, Villar2015, Paris2013, Nord2013}. 

% Between such close
% organisms, tissue dependent expression is also well
% conserved \cite{Chan2009,Brawand2011}. Similarly so for comparisons of closely related
% drosophila species \cite{Bradley2010,He2011,Paris2013,Khoueiry2017}.  Interestingly, recent work
% on these comparisons has allowed for high resolution analyses of CRE evolution and has revealed high
% rates of repurposing and TFbs turnover  \cite{Cotney2013, Reilly2015, Vierstra2014, Odom2007}.

Our working model of how regulatory elements work suggests that TFs bind on them on
specific sites. Those designated binding sites, at least the way in which we understand
them and typically model them with PWMs, are always a few base-pairs apart. Consequently
the inter-TFbs genomic positions would not be expected to be under evolutionary
pressure. On top of this, the requirements of TFs with regards to the sequence are
decently plastic. These two dynamics could explain why cis-regulatory sequence might be
more receptive to sequence alterations without a change of function.

Yet, striking exceptions do exist as a few mysterious cis-elements appear to be
ultra-conserved \cite{ultrac1,ultrac2} (over 95\% sequence similarity) in chordates
without a satisfying explanation. Why would the inter-binding site bases be conserved? 
Why are they so deeply conserved? Unfortunately for now these questions remain a mystery.
     
% If we reduce the threshold of sequence conservation to lower and more digestible
% percentages, more conserved elements are identified. This is still only a small percentage
% of the CREs that we can identify in any one biological context though so the pattern of
% faster sequence turnover in CREs holds.     

\subsection{Functional conservation}


Amphioxus shares a lot of morphological homologies with vertebrates, axial notochord, a
dorsal neural tube flanked by segmented trunk muscles, pharyngeal gill slits and a ventral
heart \cite{Martinez-Morales2016}. If development and tissue formation is defined by gene
activity, and two organisms display similar developmental and morphological patterns, it
is reasonable to expect homologous genes to be orchestrating the homologous tissue
formations and homologous cis-regulatory elements to be orchestrating the gene expression.
This is something that we have seen before in more closely related lineages but we cannot be certain that it
extends to the root of chordates.

We showed significant amounts of transcriptional conservation in chordates,
both in time and space. Gene neighborhoods \footnote{Any given gene and its top most co-expressed other genes}
from amphioxus tend to remain co-expressed in human 
(NACC analysis, Chapter \ref{chap:nacc}).
Functional modules of genes, grouped independently in amphioxus and zebrafish, tend to
share more homologous genes when they are associated to the same function (Chapter \ref{WGCNA}).
For example,
the brain module of zebrafish is most enriched in orthologous genes when compared to the
neural tube modules from amphioxus. Furthermore, amphioxus displays a phylotypic period 
(Chapter \ref{chapter:phylotyp}), a period in early development where
the genes of vertebrates, and now as we learned chordates too, are transcribed at the
most similar levels between all species.


% A perhaps more relevant approach to the conservation of CREs, than sequence conservation,
% is through functional conservation. Gene expression is controlled by cis-regulation, so if
% homologous genes in two species are expressed in similar ways, the function of the
% cis-regulation has been conserved as well.

So the conserved morphology can be seen in conserved gene transcription. Gene
transcription is controlled by cis-regulation, so if
homologous genes in two species are expressed in similar ways, the function of
cis-regulation has been conserved as well. CREs and the putative TF binding sites that they include have often been modeled as a
language. The binding sites are words and CREs are short phrases or sets of words that
carry some function in cis-regulation. There is some evidence that the 'words' are needed
in specific order, but also evidence to the contrary. 

Instead of investigating individual elements we looked at the 'words' contained in the
entire regulatory landscapes of genes. We showed that modules of genes with many shared
homologous genes and similar transcription levels, often also display a similar preference
or disinclination for the same TFs. For example, the cilium-associated modules from both
species are enriched in binding sites of RFX TFs, known to regulate ciliogenesis 
\cite{rfxcilia}.

During development too, we see the cis-regulatory content of gene lanscapes reflecting the
conservation of gene transcription levels. We showed that importance of different TFbs in
different developmental stages, is maximally similar between the two species at a point
in the middle of early development, consistent with the phylotypic period. In fact, the
cis-based phylotypic stage was detected right before the gene-based period, making it
tempting to think that we caught cause and effect in action, but since we don't have ATAC-seq experiments
for some of the intermediate stages, we can't tell for sure.

To recapitulate, we observe an overarching body plan conservation amongst chordates.
Amphioxus and vertebrates adhere to the phylotypic developmental pattern and 'use'
conserved modules of genes that are controlled by similar TFs. This is most clearly shown
for gene modules participating in specialized developmental programs, brain, cilium, skin.
It is hard to say if this means something for the programs that we do not detect to be
clearly conserved, or if this is a limitation of our approach. Even more detailed
transcriptomic datasets during development, perhaps single-cell RNA-seq and ATAC-seq 
would help shed more light into the rest of the developmental programs.

Being unable to show homology of CREs based on sequence conservation, the origin of the cis-similarities between the
species remains open to interpretation. Similar cis activity drives similar gene
expression to create similar morphologies. Did CREs get conserved and elaborated during
evolution like genes but to a degree that we cannot tell them apart? did they get lost a rediscovered? 
Maybe we don't even need to consider each CRE as functional unit, and the more vague conservation 
of the underlying vocabulary in the vicinity of genes is enough. 





% If genes are co-expressed in human and amphioxus, it means that their regulation has also
% been conserved, therefore, the function of the cis-regulatory elements (gene regulation)
% was itself conserved overall. 

% In a more targeted approach, we compared modules of co-expressed genes, as defined by
% WGCNA \ref{WGCNA}. These are sets of genes, grouped together based on transcriptomic
% sequencing data. We can investigate where the genes of each module tend to be most
% expressed and whether there's Gene Ontology (GO) enrichments for them. In some cases, this
% annotation really clarifies the module's contents. 
% For example, the two modules plotted on the left of Fig. 
% \ref{fig:tfigure_module_annotation} in chapter \ref{WGCNA}, are very clearly link to
% cilium formation. They are highly expressed in ciliated tissues/stages and show a high
% enrichment for cilium GO terms. Other modules where harder to characterize so they were
% given more vague names such as 'Translation'. 

% We found that many of the pairs of modules with similar annotations (i.e. Cilium from
% amphioxus to Cilium from zebrafish) were enriched in homologous genes, showing
% yet again the conservation of gene regulation and therefore of cis-regulation, at least
% functionally. Furthermore, we showed that for some of these pairs, the relative importance
% of putative PWM hits in the CREs assigned to their genes is correlated. In other words,
% the PWMs that are important for a module in amphioxus tend to be important for the
% 'homologous' module in zebrafish.

% Overall then, many gene expression patterns are conserved, and that is reflected on the
% content of their cis-regulatory landscapes. This overarching body plan conservation
% amongst chordates is shown once more in our phylotypic analysis \ref{chapter:phylotyp}.
% The period of maximal gene transcription similarity in chordates can only be achieved
% through conserved gene regulation and we show that this is reflected on the cis-regulatory
% content of genes.



% These results provide a regulatory framework accounting for overall body plan conservation
% 480 amongst chordates, showing that organs such as brain and liver, which have very different
% 481 rates of TF turnover and repurposing in vertebrates
% 482 regulatory conservation across chordates.




% We wanted to investigate to
% what degree the regulatory networks that are active throughout development are conserved in
% amphioxus. Vast regulatory networks are a landmark of vertebrate genomes so it is interesting to see
% to what degree this is a novelty.

% A first approach was the implementation of the NACC analysis (Chapter \ref{chap:nacc}), a method which shows
% conservation based on RNAseq assays that don't need to be in matching tissues between the
% species. We performed the analysis with human as the reference species, against mouse,
% zebrafish and amphioxus, and showed that there are indeed detectable levels or regulatory
% conservation between amphioxus and human. Given the conserved homologous tissues and
% general body-plan of the two organisms this might not be a big surprise.

% Since our dataset does allow some matching of data between species, we did so for our next analyses
% where we attempted to further investigate to what degree cis regulation is conserved
% between the two species.To make use of our developmental time series data of RNAseq and ATAC-seq in both species, our
% collaborators performed pairwise transcriptomic comparisons and showed a
% consistent period of higher transcriptomic similarity between amphioxus and all vertebrate
% species (Chapter \ref{chapter:phylotyp}).

% We extended the inquiry into the cis-regulatory context. If there's a phylum-wide
% conserved patter of organogenesis, then based on what we have discussed so far, it would
% be orchestrated by cis-regulation and a certain degree of conservation should be
% detectable there as well

% We performed another pairwise comparison for the developmental stages of our two model
% organisms, but this time based on cis regulatory content (Chapter \ref{chapter:phylotyp}). 
% We discovered dynamics consistent with the hourglass model, the cis-content of the
% developmental stages had a peak of similarity between the two organisms in the middle of
% the early development. Interestingly the two most similar stages (in terms of
% cis-regulatory content) being those directly preceding the RNA phylotypic period.
 
% Having detected conservation of cis-regulation through these two approaches, we we curious
% to see if we could identify specific regulatory networks that have been retained
% throughout the chordates. An analysis conducted for our work in \ref{ourpaper} yielded
% about two dozen modules of genes in each of our species. These are modules or clusters or
% groups of genes that, based on their transcriptomic levels on many developmental stages
% and tissues are co-expressed. By examining the expression of each of these modules in the
% various samples, but also by examining Gene Ontology enrichments for those modules, we
% manually annotated the modules with more biologically meaningful names. For example, a
% module that had high expression in testis and sperm of zebrafish and gave a GO enrichment
% for the term 'cilium organization' was named 'cilium' (Fig. 
% \ref{fig:tfigure_module_annotation}).

% We then compared the regulatory content of those modules in a cross species pairwise
% manner, and discovered that a number of gene modules involved in cilium, brain, skin,
% muscle, and more, are features that were bequeathed to chordates and not evolved in
% vertebrates.
