


# concatenate the manually made bib files with the mendeley_bibtex_sync export and move it here
cat ../mendeley_bibtex_sync/library.bib\
 ../mendeley_bibtex_sync/manual_bibtex/*.bib > ./MyThesisBibliography.bib # mv ../manual_bibtex/MyThesisBibliography.bib ./


########### REmove URLS from bibliography they are too clunky
# The command you actually need is sed 's/^\s*url.*,//' bibliography.bib >temp.bib, which breaks down to run sed 
# to substitute (s/) from the beginning of the line (^) one or more (*) whitespace characters (\s) followed by "url",
#  and one or more (*) of any character except newline (.) up to a comma. The replacement is blank 
#  (given by the // as / is the delimiter). This leaves a blank line where the url was, but bibtex won't mind.
sed 's/^\s*url.*,//' ./MyThesisBibliography.bib > temp
mv temp MyThesisBibliography.bib

sed 's/^\s*arXiv.*,//' ./MyThesisBibliography.bib > temp
mv temp MyThesisBibliography.bib




# Clear the auxiliaries
rm *.aux *.pdf *.ps *.log *.dvi *.tex~

# run once to make the auxiliary files
pdflatex main.tex


# cleans the .bib file from all the duplicate shit
# only outputs cited references
# bibexport -o temp main.aux && mv temp.bib MyThesisBibliography.bib

bibtex main.aux


# run again
pdflatex main.tex

# run again
pdflatex main.tex