\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\contentsline {chapter}{Declaration of Authorship}{v}{section*.2}
\contentsline {chapter}{Resumen}{ix}{section*.3}
\contentsline {chapter}{Abstract}{xi}{section*.4}
\contentsline {chapter}{Acknowledgements}{xiii}{section*.5}
\contentsline {chapter}{Foreword: A few words on the Thesis itself}{xvi}{section*.6}
\contentsline {chapter}{ Repository - Contact Info}{xvii}{section*.6}
\contentsline {part}{I\hspace {1em}Introduction}{1}{part.9}
\contentsline {section}{\numberline {0.1}Transcription Regulation}{5}{section.12}
\contentsline {chapter}{\numberline {1}Trans-Regulation}{7}{chapter.14}
\contentsline {section}{\numberline {1.1}DNA-protein Binding}{7}{section.15}
\contentsline {section}{\numberline {1.2}Wet lab techniques}{9}{section.19}
\contentsline {section}{\numberline {1.3}Dry lab techniques}{11}{section.24}
\contentsline {subsection}{\numberline {1.3.1}PWMs}{12}{subsection.25}
\contentsline {subsection}{\numberline {1.3.2}Other seq-based approaches}{12}{subsection.27}
\contentsline {section}{\numberline {1.4}Neural Networks}{13}{section.28}
\contentsline {subsection}{\numberline {1.4.1}How NNs work}{14}{subsection.30}
\contentsline {section}{\numberline {1.5}Other non seq-based approaches}{18}{section.37}
\contentsline {section}{\numberline {1.6}Nimrod}{20}{section.40}
\contentsline {chapter}{\numberline {2}Cis-Regulation}{23}{chapter.42}
\contentsline {section}{\numberline {2.1}Cis-Regulation}{23}{section.43}
\contentsline {subsection}{\numberline {2.1.1}Promoters}{24}{subsection.44}
\contentsline {subsection}{\numberline {2.1.2}Enhancers}{24}{subsection.45}
\contentsline {section}{\numberline {2.2}Chromatin accessibility}{25}{section.46}
\contentsline {subsection}{\numberline {2.2.1}ATAC-seq}{26}{subsection.48}
\contentsline {section}{\numberline {2.3}Histone modifications}{30}{section.53}
\contentsline {subsection}{\numberline {2.3.1}H3K4me3}{32}{subsection.55}
\contentsline {subsection}{\numberline {2.3.2}H3K27ac}{33}{subsection.56}
\contentsline {section}{\numberline {2.4}Under the light of Evolution}{33}{section.57}
\contentsline {subsection}{\numberline {2.4.1} on the Tree of life }{34}{subsection.58}
\contentsline {subsection}{\numberline {2.4.2}Amphioxus}{36}{subsection.62}
\contentsline {subsection}{\numberline {2.4.3}Whole Genome Duplications}{36}{subsection.63}
\contentsline {chapter}{\numberline {3} Objectives }{39}{chapter.65}
\contentsline {part}{II\hspace {1em}Results: The origins of vertebrate gene regulation}{41}{part.71}
\contentsline {chapter}{\numberline {4} Introductory Analyses}{43}{chapter.72}
\contentsline {section}{\numberline {4.1}The genomes}{43}{section.73}
\contentsline {section}{\numberline {4.2}Intergenic regions}{45}{section.75}
\contentsline {section}{\numberline {4.3}GREAT regions}{46}{section.78}
\contentsline {section}{\numberline {4.4}Histone Modification ChIP-seq}{47}{section.80}
\contentsline {subsection}{\numberline {4.4.1}Width of peaks}{48}{subsection.81}
\contentsline {subsection}{\numberline {4.4.2}Number of peaks/ Genome coverage}{49}{subsection.83}
\contentsline {section}{\numberline {4.5}ATAC-seq}{52}{section.87}
\contentsline {section}{\numberline {4.6}CRE-TSS distances}{53}{section.89}
\contentsline {section}{\numberline {4.7}Higher regulatory content }{55}{section.92}
\contentsline {subsection}{\numberline {4.7.1}Matched genomic region sizes}{56}{subsection.94}
\contentsline {subsection}{\numberline {4.7.2}Downsampling}{59}{subsection.97}
\contentsline {chapter}{\numberline {5} Conservation of cis regulation }{61}{chapter.99}
\contentsline {section}{\numberline {5.1} NACC }{61}{section.100}
\contentsline {section}{\numberline {5.2}The phylotypic period}{64}{section.103}
\contentsline {section}{\numberline {5.3} Gene Modules }{67}{section.107}
\contentsline {subsection}{\numberline {5.3.1}The WGCNA analysis}{67}{subsection.108}
\contentsline {subsection}{\numberline {5.3.2} Homologous Gene Content }{68}{subsection.110}
\contentsline {subsection}{\numberline {5.3.3} Cis-Regulatory Content }{70}{subsection.112}
\contentsline {chapter}{\numberline {6} Regulatory content and gene fate after WGD }{75}{chapter.116}
\contentsline {subsection}{\numberline {6.0.1} Gene Fate after WGD }{75}{subsection.117}
\contentsline {subsection}{\numberline {6.0.2} CREs per paralog }{77}{subsection.120}
\contentsline {subsection}{\numberline {6.0.3} Increased regulatory complexity in functionally specialized ohnologs }{78}{subsection.123}
\contentsline {part}{III\hspace {1em}Results: Detecting TF binding with a Neural Network}{85}{part.129}
\contentsline {section}{\numberline {6.1}Training concepts}{88}{section.131}
\contentsline {subsection}{\numberline {6.1.1}Choice of data}{88}{subsection.132}
\contentsline {subsection}{\numberline {6.1.2}Batch size}{89}{subsection.133}
\contentsline {subsection}{\numberline {6.1.3}Learning rate}{90}{subsection.134}
\contentsline {subsection}{\numberline {6.1.4}Early stopping}{90}{subsection.135}
\contentsline {subsection}{\numberline {6.1.5} Evaluating a classifier }{91}{subsection.136}
\contentsline {section}{\numberline {6.2}CTCF and p63}{92}{section.140}
\contentsline {chapter}{\numberline {7}Architecture}{95}{chapter.142}
\contentsline {section}{\numberline {7.1}The first two layers}{95}{section.143}
\contentsline {section}{\numberline {7.2}Merging the first two layers}{99}{section.152}
\contentsline {section}{\numberline {7.3}The deeper layers}{100}{section.154}
\contentsline {chapter}{\numberline {8}Training results}{105}{chapter.158}
\contentsline {section}{\numberline {8.1}Early stopping}{105}{section.159}
\contentsline {section}{\numberline {8.2}Batch size}{106}{section.161}
\contentsline {section}{\numberline {8.3}Learning rate}{107}{section.163}
\contentsline {chapter}{\numberline {9}Performance and comparison with other tools}{109}{chapter.165}
\contentsline {section}{\numberline {9.1}Cross species}{111}{section.167}
\contentsline {part}{IV\hspace {1em}Discussion}{115}{part.170}
\contentsline {chapter}{\numberline {10}Evolution of Cis regulation}{117}{chapter.171}
\contentsline {section}{\numberline {10.1}Conservation of CREs}{117}{section.172}
\contentsline {subsection}{\numberline {10.1.1}Functional conservation}{119}{subsection.173}
\contentsline {section}{\numberline {10.2}Complexity}{120}{section.175}
\contentsline {subsection}{\numberline {10.2.1}Complexity and WGD}{121}{subsection.176}
\contentsline {section}{\numberline {10.3}Fate}{124}{section.178}
\contentsline {chapter}{\numberline {11}On artificial Neural Networks and TF binding sites}{127}{chapter.179}
\contentsline {chapter}{\numberline {12} Conclusions }{131}{chapter.182}
\contentsline {part}{V\hspace {1em}Methods}{133}{part.189}
\contentsline {chapter}{\numberline {13}Notebooks}{135}{chapter.190}
\contentsline {section}{\numberline {13.1}PWMs used}{135}{section.191}
\contentsline {section}{\numberline {13.2}TF annotation and TF binding specificity prediction}{136}{section.192}
\contentsline {section}{\numberline {13.3}TF motif mapping onto ATAC-seq peaks}{137}{section.194}
\contentsline {section}{\numberline {13.4}GenomeSizes}{137}{section.195}
\contentsline {section}{\numberline {13.5}Intergenic and GREAT size distributions}{141}{section.196}
\contentsline {section}{\numberline {13.6}Make TSS files}{143}{section.197}
\contentsline {section}{\numberline {13.7}Make GREAT-like files}{148}{section.198}
\contentsline {section}{\numberline {13.8}Make Intergenic region files}{151}{section.199}
\contentsline {section}{\numberline {13.9}ChIP-seq overview}{152}{section.200}
\contentsline {section}{\numberline {13.10}ATAC-seq overview}{166}{section.201}
\contentsline {section}{\numberline {13.11}CRE-TSS distances}{172}{section.202}
\contentsline {section}{\numberline {13.12}CRE count distributions}{180}{section.203}
\contentsline {section}{\numberline {13.13}CRE count stratified}{197}{section.204}
\contentsline {section}{\numberline {13.14}Downsampling}{203}{section.205}
\contentsline {section}{\numberline {13.15}Cis-content Phylotypic}{208}{section.206}
\contentsline {section}{\numberline {13.16} Module-module comparisons }{219}{section.207}
\contentsline {section}{\numberline {13.17}NACC}{235}{section.208}
\contentsline {part}{VI\hspace {1em}Appendices}{245}{part.209}
\contentsline {section}{\numberline {13.18}Tables}{247}{section.210}
\contentsline {section}{\numberline {13.19}Nimrod data}{248}{section.211}
\contentsline {section}{\numberline {13.20}ATAC-seq data}{249}{section.214}
\contentsline {section}{\numberline {13.21}Genomes}{249}{section.217}
\contentsline {section}{\numberline {13.22}RNA assays}{249}{section.219}
\contentsline {chapter}{References}{255}{chapter*.224}
\contentsline {chapter}{List of Figures}{276}{table.226}
\contentsline {chapter}{List of Tables}{276}{table.226}
