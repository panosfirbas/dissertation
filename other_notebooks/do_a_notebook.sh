nb=$1
name=$(basename $nb )
name=${name%.ipynb}

echo "running to html " ${nb}
jupyter nbconvert --to=html --output=../VC/Appendices/${name}.html $nb
