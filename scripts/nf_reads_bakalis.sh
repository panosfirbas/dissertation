
echo "/home/ska/panos/Thesis/data/ATAC/amphi/BAM/ATAC_amphioxus_15h_rep2.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/amphi/BAM/ATAC_amphioxus_15h_rep2.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/amphi/nf_reads/ATAC_amphioxus_15h_rep2_nf_reads_{}.bed.gz"

echo "/home/ska/panos/Thesis/data/ATAC/amphi/BAM/ATAC_amphioxus_36h_rep1.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/amphi/BAM/ATAC_amphioxus_36h_rep1.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/amphi/nf_reads/ATAC_amphioxus_36h_rep1_nf_reads_{}.bed.gz"
echo "/home/ska/panos/Thesis/data/ATAC/amphi/BAM/ATAC_amphioxus_36h_rep2.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/amphi/BAM/ATAC_amphioxus_36h_rep2.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/amphi/nf_reads/ATAC_amphioxus_36h_rep2_nf_reads_{}.bed.gz"

echo "/home/ska/panos/Thesis/data/ATAC/amphi/BAM/ATAC_amphioxus_60h_rep1.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/amphi/BAM/ATAC_amphioxus_60h_rep1.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/amphi/nf_reads/ATAC_amphioxus_60h_rep1_nf_reads_{}.bed.gz"
echo "/home/ska/panos/Thesis/data/ATAC/amphi/BAM/ATAC_amphioxus_60h_rep2.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/amphi/BAM/ATAC_amphioxus_60h_rep2.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/amphi/nf_reads/ATAC_amphioxus_60h_rep2_nf_reads_{}.bed.gz"

echo "/home/ska/panos/Thesis/data/ATAC/amphi/BAM/ATAC_amphioxus_8h_rep1.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/amphi/BAM/ATAC_amphioxus_8h_rep1.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/amphi/nf_reads/ATAC_amphioxus_8h_rep1_nf_reads_{}.bed.gz"
echo "/home/ska/panos/Thesis/data/ATAC/amphi/BAM/ATAC_amphioxus_8h_rep2.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/amphi/BAM/ATAC_amphioxus_8h_rep2.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/amphi/nf_reads/ATAC_amphioxus_8h_rep2_nf_reads_{}.bed.gz"

echo "/home/ska/panos/Thesis/data/ATAC/amphi/BAM/ATAC_amphioxus_hepatic_rep1.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/amphi/BAM/ATAC_amphioxus_hepatic_rep1.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/amphi/nf_reads/ATAC_amphioxus_hepatic_rep1_nf_reads_{}.bed.gz"
echo "/home/ska/panos/Thesis/data/ATAC/amphi/BAM/ATAC_amphioxus_hepatic_rep2.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/amphi/BAM/ATAC_amphioxus_hepatic_rep2.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/amphi/nf_reads/ATAC_amphioxus_hepatic_rep2_nf_reads_{}.bed.gz"

echo "/home/ska/panos/Thesis/data/ATAC/medaka/BAM/ATAC_medaka_24h_rep1.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/medaka/BAM/ATAC_medaka_24h_rep1.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/medaka/nf_reads/ATAC_medaka_24h_rep1_nf_reads_{}.bed.gz"
echo "/home/ska/panos/Thesis/data/ATAC/medaka/BAM/ATAC_medaka_24h_rep2.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/medaka/BAM/ATAC_medaka_24h_rep2.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/medaka/nf_reads/ATAC_medaka_24h_rep2_nf_reads_{}.bed.gz"

echo "/home/ska/panos/Thesis/data/ATAC/medaka/BAM/ATAC_medaka_48h_rep1.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/medaka/BAM/ATAC_medaka_48h_rep1.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/medaka/nf_reads/ATAC_medaka_48h_rep1_nf_reads_{}.bed.gz"
echo "/home/ska/panos/Thesis/data/ATAC/medaka/BAM/ATAC_medaka_48h_rep2.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/medaka/BAM/ATAC_medaka_48h_rep2.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/medaka/nf_reads/ATAC_medaka_48h_rep2_nf_reads_{}.bed.gz"

echo "/home/ska/panos/Thesis/data/ATAC/medaka/BAM/ATAC_medaka_8som_rep1.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/medaka/BAM/ATAC_medaka_8som_rep1.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/medaka/nf_reads/ATAC_medaka_8som_rep1_nf_reads_{}.bed.gz"
echo "/home/ska/panos/Thesis/data/ATAC/medaka/BAM/ATAC_medaka_8som_rep2.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/medaka/BAM/ATAC_medaka_8som_rep2.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/medaka/nf_reads/ATAC_medaka_8som_rep2_nf_reads_{}.bed.gz"

echo "/home/ska/panos/Thesis/data/ATAC/medaka/BAM/ATAC_medaka_dome_rep1.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/medaka/BAM/ATAC_medaka_dome_rep1.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/medaka/nf_reads/ATAC_medaka_dome_rep1_nf_reads_{}.bed.gz"
echo "/home/ska/panos/Thesis/data/ATAC/medaka/BAM/ATAC_medaka_dome_rep2.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/medaka/BAM/ATAC_medaka_dome_rep2.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/medaka/nf_reads/ATAC_medaka_dome_rep2_nf_reads_{}.bed.gz"

echo "/home/ska/panos/Thesis/data/ATAC/medaka/BAM/ATAC_medaka_shield_rep1.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/medaka/BAM/ATAC_medaka_shield_rep1.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/medaka/nf_reads/ATAC_medaka_shield_rep1_nf_reads_{}.bed.gz"
echo "/home/ska/panos/Thesis/data/ATAC/medaka/BAM/ATAC_medaka_shield_rep2.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/medaka/BAM/ATAC_medaka_shield_rep2.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/medaka/nf_reads/ATAC_medaka_shield_rep2_nf_reads_{}.bed.gz"

echo "/home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_24h_rep1.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_24h_rep1.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/zebra/nf_reads/ATAC_zebra_24h_rep1_nf_reads_{}.bed.gz"
echo "/home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_24h_rep2.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_24h_rep2.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/zebra/nf_reads/ATAC_zebra_24h_rep2_nf_reads_{}.bed.gz"

echo "/home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_256c_rep1.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_256c_rep1.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/zebra/nf_reads/ATAC_zebra_256c_rep1_nf_reads_{}.bed.gz"
echo "/home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_256c_rep2.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_256c_rep2.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/zebra/nf_reads/ATAC_zebra_256c_rep2_nf_reads_{}.bed.gz"

echo "/home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_48h_rep1.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_48h_rep1.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/zebra/nf_reads/ATAC_zebra_48h_rep1_nf_reads_{}.bed.gz"
echo "/home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_48h_rep2.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_48h_rep2.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/zebra/nf_reads/ATAC_zebra_48h_rep2_nf_reads_{}.bed.gz"

echo "/home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_80ep_rep1.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_80ep_rep1.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/zebra/nf_reads/ATAC_zebra_80ep_rep1_nf_reads_{}.bed.gz"
echo "/home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_80ep_rep2.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_80ep_rep2.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/zebra/nf_reads/ATAC_zebra_80ep_rep2_nf_reads_{}.bed.gz"

echo "/home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_8som_rep1.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_8som_rep1.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/zebra/nf_reads/ATAC_zebra_8som_rep1_nf_reads_{}.bed.gz"
echo "/home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_8som_rep2.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_8som_rep2.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/zebra/nf_reads/ATAC_zebra_8som_rep2_nf_reads_{}.bed.gz"

echo "/home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_dome_rep1.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_dome_rep1.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/zebra/nf_reads/ATAC_zebra_dome_rep1_nf_reads_{}.bed.gz"
echo "/home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_dome_rep2.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_dome_rep2.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/zebra/nf_reads/ATAC_zebra_dome_rep2_nf_reads_{}.bed.gz"

echo "/home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_panc_rep1.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_panc_rep1.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/zebra/nf_reads/ATAC_zebra_panc_rep1_nf_reads_{}.bed.gz"
echo "/home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_panc_rep2.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_panc_rep2.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/zebra/nf_reads/ATAC_zebra_panc_rep2_nf_reads_{}.bed.gz"

echo "/home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_shield_rep1.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_shield_rep1.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/zebra/nf_reads/ATAC_zebra_shield_rep1_nf_reads_{}.bed.gz"
echo "/home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_shield_rep2.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_shield_rep2.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/zebra/nf_reads/ATAC_zebra_shield_rep2_nf_reads_{}.bed.gz"

echo "/home/ska/panos/Thesis/data/ATAC/amphi/BAM/ATAC_amphioxus_muscle_rep1.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/amphi/BAM/ATAC_amphioxus_muscle_rep1.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/amphi/nf_reads/ATAC_amphioxus_muscle_rep1_nf_reads_{}.bed.gz"
echo "/home/ska/panos/Thesis/data/ATAC/amphi/BAM/ATAC_amphioxus_ntube_rep1.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/amphi/BAM/ATAC_amphioxus_ntube_rep1.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/amphi/nf_reads/ATAC_amphioxus_ntube_rep1_nf_reads_{}.bed.gz"
echo "/home/ska/panos/Thesis/data/ATAC/medaka/BAM/ATAC_medaka_80epi_rep1.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/medaka/BAM/ATAC_medaka_80epi_rep1.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/medaka/nf_reads/ATAC_medaka_80epi_rep1_nf_reads_{}.bed.gz"
echo "/home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_fin_rep1.rmdup.bam"
python /home/ska/panos/Thesis/scripts/get_nf_reads.py /home/ska/panos/Thesis/data/ATAC/zebra/BAM/ATAC_zebra_fin_rep1.rmdup.bam 120 40 "/home/ska/panos/Thesis/data/ATAC/zebra/nf_reads/ATAC_zebra_fin_rep1_nf_reads_{}.bed.gz"
