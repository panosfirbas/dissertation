
nb=$1
name=$(basename $nb )
name=${name%.ipynb}

echo "running " ${nb}
jupyter nbconvert --ExecutePreprocessor.timeout=None --inplace --to notebook --execute $nb
