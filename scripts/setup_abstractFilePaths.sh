# reverse the thing
find ./ -type f -name '*.ipynb' -readable -writable -exec vim -c "%s#$PWD#THESISROOTDIR#gi" -c 'wq' {} \;

find ./fig_notebooks -type f -name '*.py' -readable -writable -exec vim -c "%s#$PWD#THESISROOTDIR#gi" -c 'wq' {} \;