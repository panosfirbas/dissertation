# Fix paths in notebooks
# these commands will find a specific text "THESISROOTDIR"
# and will replace it with the path to where the thesis repo was put
# your screen will flip out for a few seconds, don't worry
find ./ -type f -name '*.ipynb' -readable -writable -exec vim -c "%s#THESISROOTDIR#$PWD#gi" -c 'wq' {} \;
find ./fig_notebooks/ -type f -name '*.py' -readable -writable -exec vim -c "%s#THESISROOTDIR#$PWD#gi" -c 'wq' {} \;