#!/bin/bash

#SBATCH -J htseq
#SBATCH -p week
#SBATCH -N 1
#SBATCH -n 1


## Change the GTF for the corresponding organism

source /home/ska/tools/py3_venv/bin/activate
gtf_path=/home/ska/panos/Urchins/data/SpBase3.1_build8.gff3/Transcriptome.gff3
bam=$1


### IMPORTANT: --nounique allows to count reads that overlap various transcripts
htseq-count -f bam --nonunique all -i ID -t gene -s no $bam $gtf_path > ${bam%.sortedByCoord.out.bam}.counts