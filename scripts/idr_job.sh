#!/bin/bash
#
#SBATCH --job-name=incr
#SBATCH --output=slurm-fin_pk-%j.o.out
#SBATCH --error=slurm-fin_pk-%j.e.out
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --cpus-per-task=4



source /home/ska/tools/py2_venv/bin/activate

py3='/home/ska/tools/py3_venv/bin/python'
idr='/home/ska/tools/py3_venv/bin/idr'


from=${SLURM_SUBMIT_DIR}

org=$1
stage=$2
gsize=$3
mask=$4


# The nucleosome free reads have been precomputed and we can just grab them from there
ocd1=/home/ska/panos/Thesis/data/ATAC/${org}/nf_reads/ATAC_${org}_${stage}_rep1_nf_reads_*.bed.gz
ocd2=/home/ska/panos/Thesis/data/ATAC/${org}/nf_reads/ATAC_${org}_${stage}_rep2_nf_reads_*.bed.gz

# this is part of the queue job stuff
TMPDIR='/scratch/panos_'${SLURM_JOB_ID}'/'
mkdir $TMPDIR
cd $TMPDIR
mkdir ${TMPDIR}/results
res=${TMPDIR}/results/
mkdir ${TMPDIR}/data

echo 'Unpacking and shuffling'
zcat $ocd1| shuf > ./data/inp1.bed &
zcat $ocd2| shuf > ./data/inp2.bed &
wait %1 %2

echo 'Counting reads'
tr1_lines=$( cat ./data/inp1.bed | wc -l )
tr2_lines=$( cat ./data/inp2.bed | wc -l )
# we'll get half for each pseudoreplicate
tr1_half=$(( (tr1_lines + 1) / 2 ))
tr2_half=$(( (tr2_lines + 1) / 2 ))
echo 'Making pseudoreplicates'
#Make the replicate 1 pseudoreplicates
head -n ${tr1_half} ./data/inp1.bed > ./data/tr1_psr1.bed
tail -n ${tr1_half} ./data/inp1.bed > ./data/tr1_psr2.bed
#Make the replicate 2 pseudoreplicates
head -n ${tr2_half} ./data/inp2.bed > ./data/tr2_psr1.bed
tail -n ${tr2_half} ./data/inp2.bed > ./data/tr2_psr2.bed
echo 'Making Pooled pseudoreplicates'
# The Pooled pseudoreplicates
pooled_lines=$(( tr1_lines + tr2_lines ))
pool_sample_size=$(( (pooled_lines + 1) / 2 ))
cat ./data/inp1.bed ./data/inp2.bed | shuf > ./data/pooled.bed
head -n ${pool_sample_size} ./data/pooled.bed > ./data/pool_psr1.bed
tail -n ${pool_sample_size} ./data/pooled.bed > ./data/pool_psr2.bed
echo 'oke, got the pooled pseudoreplicates, ready to call peaks'


# Peak calling for replicate1, replicate 2, and pooled reads
macs2 callpeak --nomodel --keep-dup 'auto' --llocal 10000 --extsize 80 --shift -40  -p 0.02 -f BED -g ${gsize} -t ./data/inp1.bed -n ${org}_${stage}_rep1_nf --outdir ${res} &
macs2 callpeak --nomodel --keep-dup 'auto' --llocal 10000 --extsize 80 --shift -40  -p 0.02 -f BED -g ${gsize} -t ./data/inp2.bed -n ${org}_${stage}_rep2_nf --outdir ${res} &
macs2 callpeak --nomodel --keep-dup 'auto' --llocal 10000 --extsize 80 --shift -40  -p 0.02 -f BED -g ${gsize} -t ./data/inp1.bed ./data/inp2.bed -n ${org}_${stage}_comb_nf --outdir ${res} &
wait %1 %2 %3
echo 'Done with proper replicate peak calling'

#Peak call for the replicate 1 pseudoreplicates
macs2 callpeak --nomodel --keep-dup 'auto' --llocal 10000 --extsize 80 --shift -40  -p 0.02 -f BED -g ${gsize} -t ./data/tr1_psr1.bed -n ${org}_${stage}_rep1_psr1 --outdir ${res} &
macs2 callpeak --nomodel --keep-dup 'auto' --llocal 10000 --extsize 80 --shift -40  -p 0.02 -f BED -g ${gsize} -t ./data/tr1_psr2.bed -n ${org}_${stage}_rep1_psr2 --outdir ${res} &
#Peak call for the replicate 2 pseudoreplicates
macs2 callpeak --nomodel --keep-dup 'auto' --llocal 10000 --extsize 80 --shift -40  -p 0.02 -f BED -g ${gsize} -t ./data/tr2_psr1.bed -n ${org}_${stage}_rep2_psr1 --outdir ${res} &
macs2 callpeak --nomodel --keep-dup 'auto' --llocal 10000 --extsize 80 --shift -40  -p 0.02 -f BED -g ${gsize} -t ./data/tr2_psr2.bed -n ${org}_${stage}_rep2_psr2 --outdir ${res} &
wait %1 %2 %3 %4
echo 'Done with pseudo-replicate peak calling'

#Peak calling for the pooled pseudoreplicates
macs2 callpeak --nomodel --keep-dup 'auto' --llocal 10000 --extsize 80 --shift -40  -p 0.02 -f BED -g ${gsize} -t ./data/pool_psr1.bed -n ${org}_${stage}_pool_psr1 --outdir ${res} &
macs2 callpeak --nomodel --keep-dup 'auto' --llocal 10000 --extsize 80 --shift -40  -p 0.02 -f BED -g ${gsize} -t ./data/pool_psr2.bed -n ${org}_${stage}_pool_psr2 --outdir ${res} &
wait %1 %2
echo 'Done with pooled replicate peak calling'

# define some names for ease
peaks1=${res}/${org}_${stage}_rep1_nf_peaks.narrowPeak
peaks2=${res}/${org}_${stage}_rep2_nf_peaks.narrowPeak
peaks3=${res}/${org}_${stage}_comb_nf_peaks.narrowPeak
peaks_ps1=${res}/${org}_${stage}_rep1_psr1_peaks.narrowPeak
peaks_ps2=${res}/${org}_${stage}_rep1_psr2_peaks.narrowPeak
peaks_ps3=${res}/${org}_${stage}_rep2_psr1_peaks.narrowPeak
peaks_ps4=${res}/${org}_${stage}_rep2_psr2_peaks.narrowPeak
peaks_pool_ps1=${res}/${org}_${stage}_pool_psr1_peaks.narrowPeak
peaks_pool_ps2=${res}/${org}_${stage}_pool_psr2_peaks.narrowPeak

# Clean with rep mask
bedtools intersect -v -a ${peaks1} -b ${mask} -f 0.33 > ${peaks1}_masked &
bedtools intersect -v -a ${peaks2} -b ${mask} -f 0.33 > ${peaks2}_masked &
bedtools intersect -v -a ${peaks3} -b ${mask} -f 0.33 > ${peaks3}_masked &
wait %1 %2 %3
bedtools intersect -v -a ${peaks_ps1} -b ${mask} -f 0.33 > ${peaks_ps1}_masked &
bedtools intersect -v -a ${peaks_ps2} -b ${mask} -f 0.33 > ${peaks_ps2}_masked &
bedtools intersect -v -a ${peaks_ps3} -b ${mask} -f 0.33 > ${peaks_ps3}_masked &
bedtools intersect -v -a ${peaks_ps4} -b ${mask} -f 0.33 > ${peaks_ps4}_masked &
wait %1 %2 %3 %4
bedtools intersect -v -a ${peaks_pool_ps1} -b ${mask} -f 0.33 > ${peaks_pool_ps1}_masked &
bedtools intersect -v -a ${peaks_pool_ps2} -b ${mask} -f 0.33 > ${peaks_pool_ps2}_masked &
wait %1 %2


sort -k8,8nr ${peaks1}_masked | head -n 500000 > ${peaks1}_masked_f &
sort -k8,8nr ${peaks2}_masked | head -n 500000 > ${peaks2}_masked_f &
sort -k8,8nr ${peaks3}_masked | head -n 500000 > ${peaks3}_masked_f &
wait %1 %2 %3
sort -k8,8nr ${peaks_ps1}_masked | head -n 500000 > ${peaks_ps1}_masked_f &
sort -k8,8nr ${peaks_ps2}_masked | head -n 500000 > ${peaks_ps2}_masked_f &
sort -k8,8nr ${peaks_ps3}_masked | head -n 500000 > ${peaks_ps3}_masked_f &
sort -k8,8nr ${peaks_ps4}_masked | head -n 500000 > ${peaks_ps4}_masked_f &
wait %1 %2 %3 %4
sort -k8,8nr ${peaks_pool_ps1}_masked | head -n 500000 > ${peaks_pool_ps1}_masked_f &
sort -k8,8nr ${peaks_pool_ps2}_masked | head -n 500000 > ${peaks_pool_ps2}_masked_f &
wait %1 %2
echo "done with management, time for idr"

deactivate
source /home/ska/tools/py3_venv/bin/activate

# IDR:
# rep1 vs rep2, oracle: peaks from pooled sample
idr --input-file-type narrowPeak --rank p.value --soft-idr-threshold 0.1 -s ${peaks1}_masked_f ${peaks2}_masked_f -p ${peaks3}_masked_f --verbose -l ${res}/comparison1.log -o ${res}/${org}_${stage}_rep1_rep2.txt --plot &
# rep1 pseudoreplicate1 Vs rep1 pseudoreplicate2, oracle: rep1 peaks
idr --input-file-type narrowPeak --rank p.value --soft-idr-threshold 0.1 -s ${peaks_ps1}_masked_f ${peaks_ps2}_masked_f -p ${peaks1}_masked_f --verbose -l ${res}/comparison2.log -o ${res}/${org}_${stage}_psr1_psr2.txt --plot &
# rep2 pseudoreplicate1 Vs rep2 pseudoreplicate2, oracle: rep2 peaks
idr --input-file-type narrowPeak --rank p.value --soft-idr-threshold 0.1 -s ${peaks_ps3}_masked_f ${peaks_ps4}_masked_f -p ${peaks2}_masked_f --verbose -l ${res}/comparison3.log -o ${res}/${org}_${stage}_psr3_psr4.txt --plot &
# Pooled pseudoreplicates, oracle: peaks from pooled sample
idr --input-file-type narrowPeak --rank p.value --soft-idr-threshold 0.1 -s ${peaks_pool_ps1}_masked_f ${peaks_pool_ps2}_masked_f -p ${peaks3}_masked_f --verbose -l ${res}/comparison4.log -o ${res}/${org}_${stage}_poolPseudo.txt --plot &
wait %1 %2 %3 %4
echo 'DONE!'

awk 'BEGIN{OFS="\t"} $12>='"1"' {print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10}' ${res}/${org}_${stage}_rep1_rep2.txt | sort | uniq | sort -k7n,7n  > ${res}/${org}_${stage}_idr01Peaks.bed &
awk 'BEGIN{OFS="\t"} $12>='"5"' {print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10}' ${res}/${org}_${stage}_psr1_psr2.txt | bedtools intersect -v -a - -b ${res}/${org}_${stage}_idr01Peaks.bed -f 0.55 | sort | uniq | sort -k7n,7n  > ${res}/${org}_${stage}_rep1_hc.bed &
awk 'BEGIN{OFS="\t"} $12>='"5"' {print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10}' ${res}/${org}_${stage}_psr3_psr4.txt | bedtools intersect -v -a - -b ${res}/${org}_${stage}_idr01Peaks.bed -f 0.55 | sort | uniq | sort -k7n,7n  > ${res}/${org}_${stage}_rep2_hc.bed &
wait %1 %2 %3

rm ${res}/*.xls

rsync -ah ${TMPDIR}/results/* ${from}/results_${org}_${stage}_${SLURM_JOB_ID}

rm -r $TMPDIR