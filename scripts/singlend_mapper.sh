#!/bin/bash
#
#SBATCH --job-name=chipmap
#SBATCH --output=slurm-chipmapp-%j.o.out
#SBATCH --error=slurm-chipmapp-%j.e.out
#
#SBATCH --ntasks=1
#
#SBATCH --nodes=1
#SBATCH --cpus-per-task=12



export BOWTIE2_INDEXES='/scratch/indexes/bowtie2/'

samtools13=/home/ska/tools/samtools1.3/samtools

fq=${1}
basen=$(basename $1)
name=${basen%.fq}

index=${2}


from=${SLURM_SUBMIT_DIR}

TMPDIR='/scratch/panos_q'${SLURM_JOB_ID}'/'
mkdir $TMPDIR


temp_bam=${TMPDIR}${name}.bam
final_bam=${from}/${name}.rmdup.bam

bowtie2 \
-p 12 \
-x ${index} \
-U ${fq} \
--very-sensitive-local 2> ${from}/${name}_bowtie2.log | \
${samtools13} view -Shu - > ${temp_bam}

${samtools13} sort -@ 11 -T ${TMPDIR}/temp ${temp_bam} | \
${samtools13} rmdup -s - - > ${final_bam}
${samtools13} index ${final_bam}

rm ${temp_bam}

rm -r ${TMPDIR}