#!/bin/bash
#SBATCH -J STAR
#SBATCH -N 1
#SBATCH -n 25

from=${SLURM_SUBMIT_DIR}
## Change manually
indexDirectory=/scratch/indexes/star/strPur4/
gtf_path=/home/ska/panos/Urchins/data/SpBase3.1_build8.gff3/Transcriptome.gff3

echo "An RNA mapping script."
echo "Will look for STAR indexes in " $indexDirectory
echo "Will use "${gtf_path}"as the gff file"

## Don't change (except cores)
STAR=/home/ska/tools/STAR-2.5.3a/bin/Linux_x86_64_static/STAR


SCRATCH=/scratch/starmap_${SLURM_JOB_ID}
mkdir -p $SCRATCH || exit $?
mkdir $SCRATCH/starIndex
mkdir $SCRATCH/results
cd $SCRATCH


file1=$1
file2=$2
name=$3

cp $file1 $SCRATCH/1.fq
cp $file2 $SCRATCH/2.fq
cp $gtf_path $SCRATCH/model.gff3

#Add if this option if no strand specific and wanna use cufflinks
#--outSAMstrandField intronMotif
#gunzip *.fq.gz
#STAR  --genomeDir ./starIndex --outSAMtype BAM SortedByCoordinate --sjdbGTFfile model.gtf --runThreadN 10 --readFilesIn 1.fq 2.fq --outFileNamePrefix results/$namePrefix
${STAR} \
--readFilesCommand zcat \
--genomeDir ${indexDirectory}  \
--outSAMtype BAM SortedByCoordinate  \
--sjdbGTFtagExonParentTranscript Parent \
--sjdbGTFfile model.gff3 \
--runThreadN 8  \
--readFilesIn 1.fq 2.fq \
--outFileNamePrefix results/${name}  \
--quantMode GeneCounts


mv results ${from}/${name}
rm -rf $SCRATCH
