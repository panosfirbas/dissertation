# run the notebooks in place, this produces the proper out cells
# This should not be needed normally since they've already been run
# in the browser

for nb in ./fig_notebooks/*.ipynb;
do
	echo ""
	echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
	#echo $nb
	# nb=$1
	name=$(basename $nb )
	name=${name%.ipynb}

	echo "running " ${nb}
	jupyter nbconvert --ExecutePreprocessor.timeout=None --inplace --to notebook --execute $nb

	echo "###############################################################################"
	echo "###############################################################################"
done
