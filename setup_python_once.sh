

# create a local virtual environment and install necessary libraries
python3 -m venv .env && source .env/bin/activate && pip install -r requirements.txt

# symbolic link the preamble python file in the ipython folder from where it will be automatically loaded
ln -s $PWD/fig_notebooks/preamble.py $HOME/.ipython/profile_default/startup/99-thesisStuff.py

# fix paths
bash ./scripts/setup_fixFilePaths.sh



# reverse the thing
# bash ./scripts/setup_abstractFilePaths.sh