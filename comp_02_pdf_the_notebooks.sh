# compile the texes into pdfs
########################################################

cp ./other_notebooks/hybrid.png . #hacky


cd ./other/NotebookToPDFStaging/
rm *.aux *.blg *.log *.out *.pdf *.toc *.docx
cd ../../


for tex in ./other/NotebookToPDFStaging/*tex;
do
	echo $tex
	# this fixes a paths issue
	sed -i "s#/home/ska/panos/#/#" $tex

	# /home/ska/panos/myphdthesis/
	pdflatex -output-directory=./other/NotebookToPDFStaging/ $tex

	# gs -sDEVICE=pdfwrite -dProcessColorModel=/DeviceGray -dColorConversionStrategy=/Gray -dPDFUseOldCMS=false -o TMP -f ${tex%.tex}.pdf
	# mv TMP ${tex%.tex}.pdf
done

rm ./hybrid.png #hacky

cp ./other/NotebookToPDFStaging/*pdf ./VC/Appendices
########################################################