
rm ./VC/Appendices/*_bw.pdf

for pdf in ./VC/Appendices/*pdf;
do
	gs -sDEVICE=pdfwrite -dProcessColorModel=/DeviceGray -dColorConversionStrategy=/Gray -dPDFUseOldCMS=false -o TMP -f ${pdf}
	mv TMP ${pdf%.pdf}_bw.pdf
done