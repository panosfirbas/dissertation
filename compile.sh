rm ./compilation_log_1.txt 2>/dev/null
rm ./compilation_log_2.txt 2>/dev/null

# run the notebooks in place, this produces the proper out cells
########################################################
# bash comp_run_notebooks.sh



# nbconvert the notebooks to .tex files
########################################################
# bash comp_tex_the_notebooks.sh





# # Bibliography refresh
# ########################################################
# echo "Doing the bibliography"
# cat ./other/mendeley_bibtex_sync/library.bib ./other/mendeley_bibtex_sync/manual_bibtex/*.bib > ./VC/MyThesisBibliography.bib
# grep -v '^url' ./VC/MyThesisBibliography.bib > temp && mv temp ./VC/MyThesisBibliography.bib
# grep -v '^arXiv' ./VC/MyThesisBibliography.bib > temp && mv temp ./VC/MyThesisBibliography.bib


# # compile the texes into pdfs
# ########################################################
# echo "Compiling the notebooks into pdfs"
# bash comp_02_pdf_the_notebooks.sh 1>> compilation_log_1.txt 2>>compilation_log_2.txt

# echo "Draining color from notebooks"
# bash comp_03_BW_the_notebooks.sh 1>> compilation_log_1.txt 2>>compilation_log_2.txt




# Compile the book pdf
########################################################
cd VC
rm tbook*.aux tbook*.blg tbook*.log tbook*.out tbook*.pdf tbook*.toc tbook*.docx 1>> compilation_log_1.txt 2>>compilation_log_2.txt

# convert to a bad word-like file
# pandoc main.tex --bibliography=MyThesisBibliography.bib -o main.docx

echo "First pass over the tex"
xelatex tbook.tex 
echo "bibtexing it"
bibtex tbook.aux 
echo "Second pass over the tex"
xelatex tbook.tex 
mv tbook.pdf ../Thesis_inColor.pdf

echo "Any problems?"
echo '###################### "Undefined" grep '
grep undefined tbook.log
echo '###################### "Multiply defined" grep '
grep "multiply defined" tbook.log

echo "Now do it in black and white"
# Calling xelatex like this, defines the flag "doitinbw" which
# triggers some small changes in the tex files to compile the bw version of things
xelatex "\def\doitinbw{1} \input{tbook.tex}" 
mv tbook.pdf ../Thesis_BW.pdf

echo "Any problems?"
echo '###################### "Undefined" grep in '
grep undefined tbook.log
echo '###################### "Multiply defined" grep '
grep "multiply defined" tbook.log

echo "Done !"
