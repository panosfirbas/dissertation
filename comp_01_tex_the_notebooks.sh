# nbconvert the notebooks to .tex files
########################################################
# clear the folder
rm ./other/NotebookToPDFStaging/*
# convert notebooks to tex with the template
for nb in ./fig_notebooks/*.ipynb;
do
	# echo $nb
	#remove /home/ska from all notebooks
	# sed -i "s#/home/ska/panos/myphdthesis#thesis#" $nb
	bash ./scripts/nb_to_tex.sh $nb
done


# the same for "other notebooks"
for nb in ./other_notebooks/*.ipynb;
do
	bash ./scripts/nb_to_tex.sh $nb
done
########################################################