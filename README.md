<!-- ### Digital and Printed

Digital documents (DDs) have become ubiquitous for more than 20 years now.
Compared to printed documents, DDs don't consume paper or ink, can be infinitely replicated
at zero cost, can be easily edited and updated, can be made available to every person on the world
with the click of a button.      
   
Furthermore, DDs can include media like video, sound, or interactive plots, and they support searching and indexing.      
     
     
Yet, science still heavily relies on and revolves around printed documents.
Scientific publications, the smallest unit of shared research, are almost exclusively
created in the paper format, where context, research and discussion all have to fit
within the bounds of a few A4-sized pages. Why ? Because that's how things are done. 
The digital distribution of these works is most often just an afterhtought.          
     
     
Similarly, larger scientific works such as PhD Theses are expected to be formated around
the concept of a book. A book which in most cases will be printed a few dozen times for friends and family
and then stuffed in some library bookshelf never to be read again.
Admitedly printing and shelving a PhD Thesis is not a bad idea, but the author feels that the exploration
of more modern methods to disseminate science had been ignored for too long. -->
       
          


## Comparative functional genomics and artificial neural networks for the study of the evolution of cis-regulation

This is the digigital distribution of my doctoral dissertation. It is a repository that includes raw*-ish* data, 
the code for doing the analyses that were referenced in the Thesis text, and compilation scripts that can execute 
the code to produce the figures and then compile the thesis.pdf document, the actuall Thesis book that can be printed.    
     
This is possible through [free and open source software](https://www.gnu.org/philosophy/floss-and-foss.en.html).


### To run the notebooks

0. [Bedtools](https://bedtools.readthedocs.io/en/latest/) needs to be installed in your system. We won't use it directly but it is needed by a python library that we will use extensively.     

0. Download this repository
0. Uncompress the data

    ```
    >> tar vxzf data.tar.gz
    ```    

1. **Python 3 environment**    

    I very strongly recommend setting up a python virtual environment to install the required python libraries for this thesis (instead of installing them system-wide). To automatic setup a virtual environment and install the necessary libraries, try the following commands in command line (you must be in the thesis git repo root when doing that):      

    ```
    >> python3 -m venv MyNewEnv 
    >> source MyNewEnv/bin/activate 
    >> pip install -r requirements.txt
    ```    

    This virtual environment is an isolated python setup, the libraries you install in the venv will not be available for the rest of the system. To "turn it on" you need to 'source' the activate script of the virtual environment, this changes some paths in your system so that when you call 'python' the venv installation will be used instead of the system's installation. To deactivate, just type 'deactivate' in the command line, this will revert your system to default and will continue using the system's python installation.

2. The preamble    
    I've set up a file that is loaded at the start of every notebook. This is wasteful in terms of computing speed since not all of this data needs to be loaded in every notebook, but it saves a LOT of space when printing the notebooks in the Thesis book. To setup this file, I make a symbolic link of the original file in a folder of ipython (a library behind notebooks) that is automatically loaded on startup. In the command line, at the root of the repo:    

    ```bash
    >> ln -s $PWD/fig_notebooks/preamble.py $HOME/.ipython/profile_default/startup/99-thesisStuff.py
    ```    

    This way, the preamble.py script is "copied", in a way, into ipython's startup folder (with a different name since ipython expects these files to start with a number) and will be loaded at the start of every notebook that you execute.


3. *Fix absolutepaths*    
    Some paths need to be fixed in the notebooks, from the root of the thesis repo, run     

    ```bash
    # your screen will flip out for a few seconds, don't worry it's just vim opening and closing many files
    >> bash ./scripts/setup_fixFilePaths.sh
    ```    

    To revert what that script does, you can run the "./scripts/setup_abstractFilePaths.sh" script
    
        
4. Run the notebook server    
    You should now be able to run a notebook server
    
    ```bash
    # make sure to activate the virtual environment first, then
    >> jupyter notebook
    ```
    
    This will start the notebook server where you can navigate and run the notebooks


### To compile the Thesis book pdf     
If you feel like compiling the Thesis book yourself, you will need the following (and perhaps more latex-related libraries I've missed).     

* XeLaTeX     
    This is a 'latex' engine that supports more modern font types and allows us to use fonts from fonts.google.com
* Fonts:        
    These need to be installed in your system for xelatex to find and use    
    sans-serif : Roboto (https://fonts.google.com/specimen/Roboto)    
    serif : CrimsonText (https://fonts.google.com/specimen/Crimson+Text)    
* PANDOC        
    https://pandoc.org/

* Steps      
After installing the dependencies, you need to run the following scripts. The last script (compile.sh) contains the code to run the previous scripts (but it's commented out)
as well, but I think running them individually is simpler.    

    0. comp_00_run_notebooks.sh     
    This script runs all the figure-producing-notebooks. You will need to have the python3 virtualenv activated for this to work. It will create all the figures that are referenced in the thesis text and store them in the appropriate path ("thesisrootdir/Figures/from_notebooks/").     
    1. comp_01_tex_the_notebooks.sh     
    This script converts the notebooks to tex files    
    2. comp_02_pdf_the_notebooks.sh     
    This script compiles those tex files in pdf files. There is some nuance to get those just right so look into the script for some more details.     
    3. Finally the compile.sh script puts everything together to compile the Thesis book
    


